from django.urls import path, reverse_lazy
from userprofile.views import signin, signup, user, logout, information
from userprofile import views
from .views import *
#from .views import MyPasswordChangeView, MyPasswordResetDoneView
from django.contrib.auth import views as auth_views


app_name ='userprofile'
urlpatterns = [
    path('signin/', views.signin, name="signin"),
    path('signup/', views.signup, name="signup"),
    path('user/', views.user, name="user"),
    path('logout/', views.logout, name="logout"),
    path('profile/', views.profile, name="profile"),
    path('upload/', views.upload, name="upload"),
    path('success/', views.success),

    # path('change-password/', MyPasswordChangeView.as_view(), name='password-change-view'),
    # path('change-password/done/', MyPasswordResetDoneView.as_view(), name='password-change-done-view'),

    
]
