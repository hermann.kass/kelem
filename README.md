# kelem

Plateforme local de diffusion d'images

## Description du projet
Kèlèm est plateforme qui permettra au individus de poster les photos sous un format libre afin d'autres personnes puissent les utiliser ou les modifiers pour les réutiliser et les partager à nouveau.

## les Apps du projet
- Userprofile
- Images


Le projet Kèlèm est une plateforme local de diffusion d'images

## Objectifs
- avoir une banque d'images de notre environnement
- faire connaître des chasseurs d'images
- mettre en place une communauté du donné et du recevoir d'images
- valoriser les contenus locaux

## Fonctionnalités
- poster les photos
- visionner les photos
- télécharger les photos
- mettre en place une api
- api pour les dev pour afficher des speudo image

## Licence
Cette plateforme est rendu publique afin de pouvoir recvoir la contribution de tout le monde. Le programme sera disponible sous licence GPL V3

## Modélisation
### User
> ...
> picture

### Album
> name
> category_name
> image_image
> licence_name
> date_created
> update_date


### Category
> name
> date_created
> update_date

### Image
> title
> image
> desscription
> licence
> date_created
> update_date

### Licence
> name
> description
> url
> date_created
> update_date
