from django.db import models
from django.utils.translation import ugettext_lazy as _
from taggit.managers import TaggableManager

# Create your models here.
class Licence(models.Model):
    '''
    La classe Licence permettra de répertorier les différente licence libre qu'il sera possible de trouver sur la plateforme
    '''
    name = models.CharField(_("name"), max_length=150, blank=True, null=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    created_date = models.DateTimeField(_("Created date"), auto_now_add=True)
    updated_date = models.DateTimeField(_("Modified date"), auto_now=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'Licence'
        verbose_name_plural = 'Licences'

class Category(models.Model):
    '''
    La classe Category permettra de répertorier les différente categorie libre qu'il sera possible de trouver sur la plateforme
    '''
    name = models.CharField(_("name"), max_length=150, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    created_date = models.DateTimeField(_("Created date"), auto_now_add=True)
    updated_date = models.DateTimeField(_("Modified date"), auto_now=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Album(models.Model):
    '''
    La classe Album permet de classe ou organiser les images qui seront publiées
    '''
    name = models.CharField(_("name"), max_length=150, blank=True, null=True)
    category = models.ForeignKey(Category, verbose_name=_("category"), on_delete=models.CASCADE, blank=True, null=True)
    image = models.ForeignKey("Image", verbose_name=_("image"), on_delete=models.CASCADE, blank=True, null=True)
    licence = models.ForeignKey(Licence, verbose_name=_("licence"), on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    like = models.PositiveIntegerField()
    tags = TaggableManager()
    created_date = models.DateTimeField(_("Created date"), auto_now_add=True)
    updated_date = models.DateTimeField(_("Modified date"), auto_now=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'Album'
        verbose_name_plural = 'Albums'



class Image(models.Model):
    """La classe Image permettre de recevoir les fichiers image (JPG, JPEG, PNG, SVG, ...) ensuite certaine informations seront extraite du fichoier enregistré dans la base de données"""
    title = models.CharField(_("Title"), max_length=50, blank=True, null=True)
    image_file = models.ImageField(upload_to ="uploads/% Y/% m/% d/", help_text='Sélectionner les images à charger', blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    like = models.PositiveIntegerField()
    tags = TaggableManager()
    created_date = models.DateTimeField(_("Created date"), auto_now_add=True)
    updated_date = models.DateTimeField(_("Modified date"), auto_now=True)

    # Metadata
    class Meta:
        ordering = ['-image_file']
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of MyModelName."""
        return reverse('model-detail-view', args=[str(self.id)])

    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return f"{self.title}"